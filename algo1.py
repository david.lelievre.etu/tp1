#fonction pour lire les jeux de tests
import os

#fonction pour lire les jeux de tests
def read_file():
    #les jeux de tests sont dans le dossier TestsTP1
    files = os.listdir("TestsTP1")
    tests = []
    #pour chaque fichier
    for filename in files:
        #ouvrir le fichier
        file = open("TestsTP1/" + filename, "r")
        #lire le fichier
        lines = file.readlines()
        #la 1ere ligne contient les dimensions de la surface
        dimensions = lines[0].split()
        l, h = int(dimensions[0]), int(dimensions[1])
        #on recupere le nombre de points
        n = int(lines[1])
        #on recupere les points
        points = []
        for i in range(2, n+2):
            point = lines[i].split()
            points.append([int(point[0]), int(point[1])])
        
        for i in filename[:-6].split('_'):
            if i.startswith('res'):
                res = int(i[3:])

        #on ajoute le jeu de test a la liste des tests
        tests.append({'l':l, 'h':h, 'n':n, 'points':points, 'name': filename, 'resultat':res})
    #on retourne les donnees
    return tests

#algo 1 avec complexite O(n^3)
def find_max_rectangle(l, h, n, t):
    max_area = 0

    #on ajoute les points initiaux et finaux du plan dans la liste des points
    t.insert(0, [0, 0])
    t.append([l, h])

    #on parcourt tous les points pour les comparer entre deux à deux
    for i in range(len(t) - 1):
        for j in range(i + 1, len(t)):

            #la largeur maximale possible pour un rectangle situé entre ces 2 points est la distance entre les deux points
            width_possible = abs(t[j][0] - t[i][0])
            # Initialise la hauteur maximale possible à la hauteur du plan
            height_possible = h

            #on parcourt tous les points pour savoir si un point est situé entre les deux points d'indice i et j
            for k in range(0, len(t)):
                #si le point est situé entre les deux points d'indice i et j
                if t[k][0] > t[i][0] and t[k][0] < t[j][0]:

                    #on met à jour la hauteur maximale possible avec la hauteur du point
                    height_possible = min(height_possible, t[k][1])  

            #on calcule l'aire maximale possible pour un rectangle situé entre les deux points d'indice i et j
            area_possible = width_possible * height_possible
            #si l'aire maximale possible est supérieure à l'aire maximale actuelle, on met à jour l'aire maximale
            max_area = max(max_area, area_possible)

    return max_area

#algo 2 avec complexite O(n^2)
def find_max_rectangle2(l, h, n, t):
    max_area = 0

    t.insert(0, [0, 0])
    t.append([l, h])

    for i in range(len(t) - 1):
        
        #cette fois-ci, on intialise la hauteur max possible à chaque itération de i car on ne parcourt plus tous les points pour trouver la hauteur max possible
        height_possible = h

        for j in range(i + 1, len(t)):
            width_possible = abs(t[j][0] - t[i][0])

            area_possible = width_possible * height_possible
            max_area = max(max_area, area_possible)

            #si la hauteur du point est inférieure à la hauteur max possible, on met à jour la hauteur max possible pour les prochains rectangles possibles
            if t[j][1] < height_possible:
                height_possible = t[j][1]

    return max_area

#algo 3 :diviser pour regner, on va couper le plan en deux partis sur l'axe des x, au niveau du point d'ordonnée le plus bas
#on va ensuite calculer l'aire maximale possible pour un rectangle situé dans la partie gauche du plan, puis dans la partie droite du plan, il nous restera à traiter uniquement l'unique rectangle possible entre les deux parties du plan qui aura une hauteur égale à la hauteur du point d'ordonnée la plus basse et une largeur égale à la largeur du plan
def biggest_rectangle(points,height):
    if len(points) == 2: # Cas de base, on a que deux point qui servent d'extremité à l'espace.
        area = (points[-1][0]-points[0][0])*height
        return area # Seul réctangle possible dans cet condition
    elif len(points) > 2: # Cas recursif 
        # On cherche le point le plus bas
        lowest_coord = height
        for i in range(1,len(points)-1):
            if points[i][1] <= lowest_coord:
                lowest_coord = points[i][1]
                pivot_index = i
        area = (points[-1][0]-points[0][0])*lowest_coord
        return max(
            biggest_rectangle(points[0:pivot_index+1],height), # On appelle la fonction de manière recursif du point d'extremité gauche au point pivot inclus
            biggest_rectangle(points[pivot_index:],height), # On appelle la fonction de manière recursif du point pivot au point d'extremité droite inclus
            area # Le réctangle "pont" inscrit sur toute la longeur de l'espace est d'auteur du plus bas point
        )
    else: assert False, "Fonctionnement anormal de la fonction"

if __name__=='__main__':
    tests = read_file()
    for test in tests:
        n=test['n']
        l=test['l']
        h=test['h']
        points=test['points']
        resultat=test['resultat']
        testname=test['name']


        try:
            print(f'testing {testname} ... ',end='')
            area = biggest_rectangle([[0,0],*points,[l,0]],h) 
            if area == resultat:
                print(f'good')
            else:
                print(f'wrong found {area} instead of {resultat}')
        except:
            print(f'ran into recursion depth error')
